<!-- writeme -->
Expand-fraction
===============

Expand (complicate, complexify) a fraction to larger numerators and denominators with equivalent ratios.

 * Package name: agaric/expand-fraction


### Maintainers

 * Benjamin Melançon - ben@agaric.coop


### Requirements

 * phospr/fraction dev-master


### License

GPL-2.0-or-later

<!-- endwriteme -->

### Example usage

```php
<?php

require_once 'vendor/autoload.php';
require_once 'src/ExpandFraction.php';

use Agaric\ExpandFraction;

$f = new ExpandFraction(2000,725);
print_r($f->expand());
```

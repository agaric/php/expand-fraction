<?php

namespace Agaric;

use Phospr\Fraction;

class ExpandFraction {

  /**
   * fraction
   *
   * @var Fraction
   */
  public $fraction;

  /**
   * __construct
   *
   * @param integer $numerator
   * @param integer $denominator
   */
  public function __construct($numerator, $denominator = 1) {
    $this->fraction = new Fraction($numerator, $denominator);
  }

  /**
   * Expand x times.
   *
   * That is, transform 1/2 into 2/4, 3/6, 4/8, 5/10, 6/12, 7/14, 8/16 etc.
   */
  public function expand($x = 100, $start = 1) {
    $expansions = [];
    for ($i = $start; $i <= $x + $start; $i++) {
      $expansions[] = [$this->fraction->getNumerator() * $i, $this->fraction->getDenominator() * $i];
    }
    return $expansions;
  }

  /**
   * @TODO add expandRangeNumerator and expandRangeDenominator which take a
   * maximum and optional minimum number to show fractions which have parts
   * within that range.
   */
}

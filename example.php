<?php

require_once 'vendor/autoload.php';
require_once 'src/ExpandFraction.php';

use Agaric\ExpandFraction;

$f = new ExpandFraction(2000,725);
print_r($f->expand());
